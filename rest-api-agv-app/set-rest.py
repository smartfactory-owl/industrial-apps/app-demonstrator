import requests
import os
import asyncio

"""
RestClient class
"""
class RestClient:
    def __init__(self, server_url: str, basic_auth_token: str) -> None:
        self.server_url = server_url
        self.headers = {
                'accept': 'text/plain',
                'Authorization': f'Basic {basic_auth_token}',
                'Accept-Language': 'en_US',
                'Accept': 'text/plain',
            }

    async def post(self, path, data):
        try:
            print(f"[Debug] POST {self.server_url + path}\n {self.headers}\n {data}")
            response = requests.post(url=self.server_url + path, headers=self.headers, json=data)
            return response
        except requests.exceptions.ConnectionError:
            print("[Error] Connection Refused Error")
            return None
        
    async def get(self, path):
        try:
            print(f"[Debug] GET {self.server_url + path}\n {self.headers}")
            response = requests.get(url=self.server_url + path, headers=self.headers)
            return response
        except requests.exceptions.ConnectionError:
            print("[Error] Connection Refused Error")
            return None
    
"""
Get environment variable with default value
@param env: Environment variable
@param default: Default value
@return: Environment variable value or default value
"""
def get_env_variable(env: str, default = None) -> (str | None):
    env_var = os.environ.get(env)
    if env_var is None or env == '':
        if default is None:
            raise ValueError(f'[Error] {env} is not defined!')
        else:
            env_var = default
    print(f'{env} = {env_var}')
    return env_var


"""
Main function
"""
async def main():
    url = get_env_variable("REST_API_URL")
    basic_auth_token = get_env_variable("REST_API_BASIC_AUTH_TOKEN")
    mission_id = get_env_variable("MISSION_ID")

    client: RestClient = RestClient(url, basic_auth_token)
    
    # response = await client.get("/api/v2.0.0/missions")
    response = await client.post("/api/v2.0.0/mission_queue", {"mission_id": mission_id})
    
    if response is not None:
        print("\nResponse", response, response.json())
    else:
        print("Response is None")

if __name__ == "__main__":
    asyncio.run(main())