# Concept

## REST API
get mission guid from rest:
http://192.168.5.202/api/v2.0.0/missions

start mission HannoverMesse2024 via rest:
curl -X POST "http://192.168.5.202/api/v2.0.0/mission_queue" -H  "accept: application/json" -H  "Authorization: Basic ZGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==" -H  "Accept-Language: en_US" -H  "Content-Type: application/json" -d "{  \"mission_id\": \"367c49e0-f5b6-11ee-86d2-0001297c2238\"}"

abort mission:
curl -X DELETE "http://192.168.5.202/api/v2.0.0/mission_queue" -H  "accept: application/json" -H  "Authorization: Basic ZGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==" -H  "Accept-Language: en_US"

documented here:
http://192.168.5.202/doc/api/?lang=en_US&auth=ZGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==&style=robot#/Missions/get_missions

### Test Python
REST_API_URL=http://127.0.0.1 REST_API_BASIC_AUTH_TOKEN=ZGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA== python3 set-rest.py

<!-- docker rm -f control-rest-container && -->
docker build -t control-rest . &&
docker rm -f control-rest-container && docker run --name control-rest-container control-rest sleep infinity

## OPC UA
Programme = 0...3 (0=Stop, 1=Sorter, 2=Stacker, 3=DemoCycle)
opc.tcp://192.168.5.201:4840
Set Variable (int): program_auswahl
Get Variamle (int): program_status