import os
from typing import NoReturn
import asyncio
from asyncua import Client, ua

MAX_RETRY_COUNT = 0

# Main function
async def main() -> NoReturn:
    global MAX_RETRY_COUNT  # Declare MAX_RETRY_COUNT as a global variable

    # Get environment variables
    opcua_server_url = get_env_variable("OPCUA_SERVER_URL")
    program_id = int(get_env_variable("PROGRAM_ID"))
    print(f"[Env] OPC UA Server URL: {opcua_server_url}")
    print(f"[Env] Program ID: {program_id}")

    # Create client and connect to server
    client: Client = Client(url=opcua_server_url)
    try:
        await client.connect()
        print("Client connected")

        # Check if program_id is 0, program_status does not need to be checked, as any program will be stopped
        if program_id == 0:
            print("Program_id is 0. Stopping program...")
        else:

        # Check if program_auswahl is 0
            print(f"[Check] Checking if program_auswahl is set to '0'...")
            node_program_auswahl = client.get_node('ns=2;s=program_auswahl')
            program_auswahl = await node_program_auswahl.read_value()
            if not program_auswahl == 0:
                print(f"[Check][Exit] Error: 'ns=2;s=program_auswahl' ({program_auswahl}) is not 0. Robot is not idle. Start Stop App first. Exiting...")
                return
            print(f"[Check][Continue] program_auswahl is set to 0.")

        # program_id is not 0. Wait for program_status to become 0
            while True:
                print("[Await] Checking if program is idle...")
                node_program_status = client.get_node('ns=2;s=program_status')
                program_status = await node_program_status.read_value()
                print(f"[Await] Got Program status id: {program_status}")
                if program_status == 0:
                    print("[Await] Break. Program is idle. Can start new program.")
                    break
                else:
                    print(f"[Await] Program is not idle. Waiting for 1 second...")
                await asyncio.sleep(1)  # sleep for 1 second

            print(f"Starting nonzero program '{program_id}'")

        # Set program_auswahl to program_id
        print(f"[Write] Setting program_auswahl to '{program_id}'")
        data_value = ua.DataValue(ua.Variant(int(program_id), ua.VariantType.Int32))
        node_program_auswahl = client.get_node('ns=2;s=program_auswahl')
        await node_program_auswahl.write_value(data_value)

        # Wait for 5 seconds
        print("[Sleep] Waiting for 5 seconds...")
        await asyncio.sleep(5)  # sleep for 1 second

        # Check if program_auswahl is set to program_id
        print(f"[Check] Checking if program_auswahl is set to '{program_id}'...")
        program_auswahl = await node_program_auswahl.read_value()
        if program_auswahl == program_id:
            print(f"[Check][SUCCESS] program_auswahl has been set to program_id '{program_id}'.")
        else:
            MAX_RETRY_COUNT = MAX_RETRY_COUNT + 1
            if MAX_RETRY_COUNT > 3:
                print(f"[Check][Failure] Maximum retry count {MAX_RETRY_COUNT} reached, but program_auswahl is not program_id. Check if the Program is running on the Robot! Exiting...")
                return
            
            print(f"[Check][Retry] Error: 'ns=2;s=program_auswahl' ({program_auswahl}) is not equal to program_id ({program_id}). Program has not been set to program_auswahl. Retrying {MAX_RETRY_COUNT}...")
            await main()

        # # Check if program has been started/stopped using program_status
        # print(f"[Check] Checking if program has been {'stopped' if program_id == 0 else 'started'}...")
        # node = client.get_node('ns=2;s=program_status')
        # program_status = await node.read_value()
        # if program_status == program_id:
        #     print(f"[Check][SUCCESS] Program {program_id} has been started/stopped. Exiting...")
        #     return
        # else:
        #     print(f"[Check][Retrying] Error: 'ns=2;s=program_status' ({program_status}) is not equal to program_id ({program_id}). Program has not been started. Retrying... {MAX_RETRY_COUNT}")
        #     MAX_RETRY_COUNT += 1
        #     if MAX_RETRY_COUNT == 3:
        #         print(f"[Check][Failure] Maximum retry count {MAX_RETRY_COUNT} reached. Check if the Program is running on the Robot! Exiting...")
        #         return
        #     await main()

    except asyncio.exceptions.TimeoutError as e:
        print("[Error] Timeout Error: ", e)
        return
    except ConnectionRefusedError as e:
        print("[Error] Connection Refused Error: ", e)
        return
    except Exception as e:
        print("[Error] General Exception: ", e)
        return
    finally:
        await client.disconnect()
        return

# Get environment variable with default value
def get_env_variable(env: str, default = None) -> (str | None):
    env_var = os.environ.get(env)
    if env_var is None or env == '':
        if default is None:
            raise ValueError(f'[Error] {env} is not defined!')
        else:
            env_var = default
    return env_var

# Entry point
if __name__ == "__main__":
    asyncio.run(main())
